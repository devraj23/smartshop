import React from 'react';
const ContactUs = () => {
  return (
    <div className="contct">
      <h1>Contact Us</h1>
      <span>Return address for online orders:</span>
      <p>Bohemian Traders 3 / 13 Bonnal Rd Erina, NSW, 2250</p>
    </div>
  );
};
export default ContactUs;
